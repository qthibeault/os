all: dir loader kernel

dir:
	if [ ! -d build/ ]; then mkdir build build/loader build/kernel; fi;

loader: src/asm/boot_sect.asm
	nasm src/asm/boot_sect.asm -f bin -o build/loader/loader.bin

kernel: src/asm/kernel_entry.asm src/c/kernel.c
	

run: all
	qemu-system-i386 build/loader/loader.bin

clean:
	rm -r build/
