# LEAPING LYNX #

### Mission Statement: ###
Looking to understand the theory and application behind the development of a modern 32bit operating system. Working to learn low-level systems programming. Working to learn Assembly.

### End Goals: ###
Multi-user functionality
Shell interface
Basic GUI
Extensible kernel through use of module system
Proper documentation to allow subsequent explanation of developed operating system. 
