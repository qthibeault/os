bits 32

VIDEO_MEMORY equ 0xB8000
WHITE_ON_BLACK equ 0x0f
CHARS equ 2000

;******************************************;
;32 bit print function
;******************************************;

print_string_pm:
    pusha
    mov edx, VIDEO_MEMORY

print_string_pm_loop:
    mov al, [ebx]
    mov ah, WHITE_ON_BLACK
    cmp al, 0
    je print_string_pm_done
    mov [edx], ax
    add ebx, 1
    add edx, 2
    jmp print_string_pm_loop

print_string_pm_done:
    popa
    ret

;*****************************************;
;function to clear screen
;*****************************************;

clear_screen:
    pusha
    mov edx, VIDEO_MEMORY
    mov ebx, 1
    mov al, " "
    mov ah, WHITE_ON_BLACK
loop_clear_screen:
    cmp ebx, CHARS
    je done_clear_screen
    mov [edx], ax
    add edx, 2
    add ebx, 1
    jmp loop_clear_screen
done_clear_screen:
    popa
    ret