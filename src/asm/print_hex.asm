print_hex:
 mov ah, 0x0e
 lea bx, [hex_table]
 ;print high nibble on high byte
 mov al, dh
 shr al, 4
 xlat
 int 0x10
 ;print low nibble on high byte
 mov al, dh
 and al, 0x0f
 xlat
 int 0x10
 ;print high nibble on low byte
 mov al, dl
 shr al, 4
 xlat
 int 0x10
 ;print low nibble on low byte
 mov al, dl
 and al, 0x0f
 xlat
 int 0x10
 ret
 
hex_table: 
 db "0123456789ABCDEF"