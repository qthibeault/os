;*******************************************;
; Print Function for Real Mode - 16 bit
;*******************************************;

Print16:
	pusha
Loop_Print_16:
	lodsb
	or	al, al
	jz	Print16Done
	mov	ah,	0eh
	int	10h
	jmp	Loop_Print_16
Print16Done:
	popa
	ret

