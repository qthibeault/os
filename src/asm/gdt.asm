bits 16

;***************************************
;routine to install GDT
;***************************************

InstallGDT:
    cli
    pusha
    lgdt [toc]
    sti
    popa
    ret

;****************************************
;GDT definitions
;****************************************

gdt_data:
    dd 0
    dd 0

;gdt code:
    dw 0FFFFh
    dw 0
    db 0
    db 10011010b
    db 11001111b
    db 0

;gdt data:
    dw 0FFFFh
    dw 0
    db 0
    db 10010010b
    db 11001111b
    db 0
    
end_of_gdt:
;*******************************************
;pointer to the structure of the gdt
;*******************************************
toc:
    dw end_of_gdt - gdt_data - 1
    dd gdt_data